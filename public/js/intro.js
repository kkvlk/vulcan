$(function() {
  var $body = $("body");
  var $intro = $("section#intro");

  $body.addClass("frozen");
  $intro.show();

  $("a#start").on("click", function() {
    $body.removeClass("frozen");
    $intro.addClass("hidden");

    setTimeout(function() {
      $intro.hide();
    }, 1000);
  });
});
