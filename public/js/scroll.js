var $scolledElements = $('[scrolled], #tech-icons i, #bio position');
var $window = $(window);

function scrollCheck() {
  var windowHeight = $window.innerHeight();
  var windowTopPosition = $window.scrollTop();
  var windowBottomPosition = (windowTopPosition + windowHeight);

  $.each($scolledElements, function() {
    var $elem = $(this);
    var elemHeight = $elem.outerHeight();
    var elemTopPosition = $elem.offset().top;
    var elemBottomPosition = (elemTopPosition + elemHeight);
    var elemActualTopPosition = elemTopPosition - windowTopPosition;
    var elemVisibleHeight = windowHeight - elemActualTopPosition;

    if (elemVisibleHeight > elemHeight) {
      elemVisibleHeight = elemHeight;
    } else if (elemVisibleHeight < 0) {
      elemVisibleHeight = 0;
    }

    var elemLastVisibleHeightData = $elem.attr("data-visible-height");
    var elemLastVisibleHeight = parseFloat(elemLastVisibleHeightData == undefined ? 0 : elemLastVisibleHeightData);
    var visibleHeightDiff = elemVisibleHeight - elemLastVisibleHeight;

      $elem.attr("data-visible-height", elemVisibleHeight);

      if ((elemBottomPosition >= windowTopPosition) &&
          (elemTopPosition <= windowBottomPosition)) {
        $elem.attr("visible", true)

        if ((elemActualTopPosition >= (windowHeight / 2) - (windowHeight / 8)) &&
            (elemActualTopPosition <= (windowHeight / 2) + (windowHeight / 8))) {
          $elem.attr("perceived", "true");
        } else {
          $elem.removeAttr("perceived");
        }
      } else {
        $elem.removeAttr("visible").removeAttr("perceived");
      }

      if (elemActualTopPosition < 0) {
        if (elemHeight > elemVisibleHeight) {
          $elem.attr("reached", true);
          $elem.removeAttr("finished");
        } else {
          $elem.attr("finished", true)
          $elem.removeAttr("reached");
        }
      } else {
        $elem.removeAttr("reached").removeAttr("finished");
      }
  });
}

$window.on('scroll', scrollCheck);
$window.on('scroll resize', scrollCheck);

$(function() {
  $window.trigger('scroll');
});
