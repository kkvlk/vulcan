const NOMAD_LIST = [
  'Poland',
  'Uruguay',
  'Germany',
  'Argentina',
  'South Korea',
  'Spain',
];

function initNomadMap() {
  var geocoder = new google.maps.Geocoder();
  var worldMapElem = document.getElementById('nomad-map');
  var map = new google.maps.Map(worldMapElem, {
    zoom: 2,
    scrollwheel: false,
    disableDefaultUI: true,
    backgroundColor: '#6b2122',
    styles: MAP_STYLES,
    center: {
      lat: 0,
      lng: 0
    }
  });

  for (var i in NOMAD_LIST) {
    (function(address) {
      geocoder.geocode({address: address}, function(results, status) {
        if (status == 'OK') {
          var marker = new google.maps.Marker({
            position: results[0].geometry.location,
            optimized: false,
            map: map,
            icon: {
              path: google.maps.SymbolPath.CIRCLE,
              strokeWeight: 5,
              strokeColor: '#cc9900',
              scale: 7
            }
          });
        }
      });
    })(NOMAD_LIST[i]);
  }
}
